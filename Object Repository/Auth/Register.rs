<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Register</name>
   <tag></tag>
   <elementGuidId>1d92070d-5f96-428f-9426-5a16bda144b2</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;membername&quot;,
      &quot;value&quot;: &quot;${name}&quot;
    },
    {
      &quot;name&quot;: &quot;membermail&quot;,
      &quot;value&quot;: &quot;${mail}&quot;
    },
    {
      &quot;name&quot;: &quot;memberphone&quot;,
      &quot;value&quot;: &quot;${phone}&quot;
    },
    {
      &quot;name&quot;: &quot;memberpassword&quot;,
      &quot;value&quot;: &quot;${password}&quot;
    },
    {
      &quot;name&quot;: &quot;memberconfirmpassword&quot;,
      &quot;value&quot;: &quot;${confirmpassword}&quot;
    },
    {
      &quot;name&quot;: &quot;membercodereferal&quot;,
      &quot;value&quot;: &quot;${codereferral}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${url}/member/register</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(15, 1)</defaultValue>
      <description></description>
      <id>812e0873-bd06-4e35-b9ca-0a306f62fa9e</id>
      <masked>false</masked>
      <name>name</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(16, 1)</defaultValue>
      <description></description>
      <id>cf6422e5-3688-4e00-91ba-0cede1edd87f</id>
      <masked>false</masked>
      <name>mail</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(17, 1)</defaultValue>
      <description></description>
      <id>19073ac5-a70e-4f9f-8b29-e342b94056a6</id>
      <masked>false</masked>
      <name>phone</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(18, 1)</defaultValue>
      <description></description>
      <id>eca5ab5b-9e36-4cec-9c4d-496f72ec8843</id>
      <masked>false</masked>
      <name>password</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(19, 1)</defaultValue>
      <description></description>
      <id>9004e0cd-035d-466f-b88f-eac5aa9a5d4f</id>
      <masked>false</masked>
      <name>confirmpassword</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(20, 1)</defaultValue>
      <description></description>
      <id>0746f595-e56c-4803-9f9f-b5ee6a1affd6</id>
      <masked>false</masked>
      <name>codereferral</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.apiurl</defaultValue>
      <description></description>
      <id>448ce714-bf88-40c8-a978-5ae672658260</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
