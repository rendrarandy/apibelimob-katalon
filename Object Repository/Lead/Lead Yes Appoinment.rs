<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Lead Yes Appoinment</name>
   <tag></tag>
   <elementGuidId>67a18649-a675-4ed5-b011-9d1e4bcbf9ed</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;minimumpricemrp&quot;,
      &quot;value&quot;: &quot;${minimumpricemrp}&quot;
    },
    {
      &quot;name&quot;: &quot;maximumpricemrp&quot;,
      &quot;value&quot;: &quot;${maximumpricemrp}&quot;
    },
    {
      &quot;name&quot;: &quot;leadappointmentdate&quot;,
      &quot;value&quot;: &quot;${leadappointmentdate}&quot;
    },
    {
      &quot;name&quot;: &quot;leadappointmentaddress&quot;,
      &quot;value&quot;: &quot;${leadappointmentaddress}&quot;
    },
    {
      &quot;name&quot;: &quot;leadappointmentprovince&quot;,
      &quot;value&quot;: &quot;${leadappointmentprovince}&quot;
    },
    {
      &quot;name&quot;: &quot;leadappointmentcity&quot;,
      &quot;value&quot;: &quot;${leadappointmentcity}&quot;
    },
    {
      &quot;name&quot;: &quot;leadappointmentdistrict&quot;,
      &quot;value&quot;: &quot;${leadappointmentdistrict}&quot;
    },
    {
      &quot;name&quot;: &quot;leadappointmentbuilding&quot;,
      &quot;value&quot;: &quot;${leadappointmentbuilding}&quot;
    },
    {
      &quot;name&quot;: &quot;leadappointmentremark&quot;,
      &quot;value&quot;: &quot;${leadappointmentremark}&quot;
    },
    {
      &quot;name&quot;: &quot;leadreferencecode&quot;,
      &quot;value&quot;: &quot;${leadreferencecode}&quot;
    },
    {
      &quot;name&quot;: &quot;leadid&quot;,
      &quot;value&quot;: &quot;${leadid}&quot;
    },
    {
      &quot;name&quot;: &quot;pricemrp&quot;,
      &quot;value&quot;: &quot;${pricemrp}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${key}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${url}/data//lead/appointment/yes</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.apiurl</defaultValue>
      <description></description>
      <id>e4354d8a-3d14-4c08-b9c2-14313cff849c</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.tokendms</defaultValue>
      <description></description>
      <id>e5447574-6352-4321-af04-81490d8961ab</id>
      <masked>false</masked>
      <name>key</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(47, 1)</defaultValue>
      <description></description>
      <id>c3c4dcac-cc1c-4adf-a008-5b54a49e5292</id>
      <masked>false</masked>
      <name>minimumpricemrp</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(48, 1)</defaultValue>
      <description></description>
      <id>4c5baac1-9703-45a0-b2be-d76219182b75</id>
      <masked>false</masked>
      <name>maximumpricemrp</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(49, 1)</defaultValue>
      <description></description>
      <id>4bb3f6b0-560c-409e-ac92-6564c53b3709</id>
      <masked>false</masked>
      <name>pricemrp</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(51, 1)</defaultValue>
      <description></description>
      <id>5e1e8a58-d658-4d9e-9420-7cf3303ed0c0</id>
      <masked>false</masked>
      <name>leadappointmentdate</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(52, 1)</defaultValue>
      <description></description>
      <id>f8655317-0143-4af7-a0a3-9c561a80697c</id>
      <masked>false</masked>
      <name>leadappointmentaddress</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(4, 2)</defaultValue>
      <description></description>
      <id>b76349b9-d389-4c5e-8e15-b8a02908ce7f</id>
      <masked>false</masked>
      <name>leadappointmentprovince</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(5, 2)</defaultValue>
      <description></description>
      <id>f239fcc5-c36f-4e89-a813-76a4b55eb02e</id>
      <masked>false</masked>
      <name>leadappointmentcity</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(6, 2)</defaultValue>
      <description></description>
      <id>2e7c4bb8-4fe7-4161-858a-53e3093ba70b</id>
      <masked>false</masked>
      <name>leadappointmentdistrict</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(53, 1)</defaultValue>
      <description></description>
      <id>705cd3ad-d27b-401e-aae3-c84839fc1f68</id>
      <masked>false</masked>
      <name>leadappointmentbuilding</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(54, 1)</defaultValue>
      <description></description>
      <id>829be5eb-1463-4c63-82a8-93b55fc11bab</id>
      <masked>false</masked>
      <name>leadappointmentremark</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(35, 1)</defaultValue>
      <description></description>
      <id>9e852a2f-ac8f-47a8-910c-4d19168557ea</id>
      <masked>false</masked>
      <name>leadreferencecode</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(50, 1)</defaultValue>
      <description></description>
      <id>451ebe6c-2050-4726-bd18-0d53ba91626b</id>
      <masked>false</masked>
      <name>leadid</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
