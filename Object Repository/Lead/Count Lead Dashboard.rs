<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Count Lead Dashboard</name>
   <tag></tag>
   <elementGuidId>804cc42f-703e-4d1b-8d23-9520d06ca1d4</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent></httpBodyContent>
   <httpBodyType></httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${key}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>GET</restRequestMethod>
   <restUrl>${url}/data/lead/dashboard/member/${membercode}?m=${bulan}&amp;y=${tahun}</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.apiurl</defaultValue>
      <description></description>
      <id>79347c9f-65cc-4144-baf3-76be46b7a7b1</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.tokendms</defaultValue>
      <description></description>
      <id>e8e7cce5-fc1e-4b85-9418-b2bd7f078757</id>
      <masked>false</masked>
      <name>key</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(3, 2)</defaultValue>
      <description></description>
      <id>9e8d49a2-bd29-4c06-b932-0c3c7a5eca84</id>
      <masked>false</masked>
      <name>membercode</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(33, 1)</defaultValue>
      <description></description>
      <id>79224470-c356-4be1-9d9f-637e26479ce2</id>
      <masked>false</masked>
      <name>bulan</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(34, 1)</defaultValue>
      <description></description>
      <id>a685e825-9fef-4943-bfaf-bad366846c19</id>
      <masked>false</masked>
      <name>tahun</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
