<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Add Lead</name>
   <tag></tag>
   <elementGuidId>c5afa373-c130-4693-9aa3-9b996298e342</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;leadcarbrand&quot;,
      &quot;value&quot;: &quot;${leadcarbrand}&quot;
    },
    {
      &quot;name&quot;: &quot;leadcarmodel&quot;,
      &quot;value&quot;: &quot;${leadcarmodel}&quot;
    },
    {
      &quot;name&quot;: &quot;leadcarvariant&quot;,
      &quot;value&quot;: &quot;${leadcarvariant}&quot;
    },
    {
      &quot;name&quot;: &quot;leadcarnoteother&quot;,
      &quot;value&quot;: &quot;${leadcarnoteother}&quot;
    },
    {
      &quot;name&quot;: &quot;leadcaryear&quot;,
      &quot;value&quot;: &quot;${leadcaryear}&quot;
    },
    {
      &quot;name&quot;: &quot;leadcartransmission&quot;,
      &quot;value&quot;: &quot;${leadcartransmission}&quot;
    },
    {
      &quot;name&quot;: &quot;leadcarkm&quot;,
      &quot;value&quot;: &quot;${leadcarkm}&quot;
    },
    {
      &quot;name&quot;: &quot;leadcarprefixplate&quot;,
      &quot;value&quot;: &quot;${leadcarprefixplate}&quot;
    },
    {
      &quot;name&quot;: &quot;leadcarplatenumber&quot;,
      &quot;value&quot;: &quot;${leadcarplatenumber}&quot;
    },
    {
      &quot;name&quot;: &quot;leadcarcolour&quot;,
      &quot;value&quot;: &quot;${leadcarcolour}&quot;
    },
    {
      &quot;name&quot;: &quot;leadcarcolourother&quot;,
      &quot;value&quot;: &quot;${leadcarcolourother}&quot;
    },
    {
      &quot;name&quot;: &quot;leadstnktaxexpired&quot;,
      &quot;value&quot;: &quot;${leadstnktaxexpired}&quot;
    },
    {
      &quot;name&quot;: &quot;leadstnktype&quot;,
      &quot;value&quot;: &quot;${leadstnktype}&quot;
    },
    {
      &quot;name&quot;: &quot;leadfacelift&quot;,
      &quot;value&quot;: &quot;${leadfacelift}&quot;
    },
    {
      &quot;name&quot;: &quot;regionid&quot;,
      &quot;value&quot;: &quot;${regionid}&quot;
    },
    {
      &quot;name&quot;: &quot;regionname&quot;,
      &quot;value&quot;: &quot;${regionname}&quot;
    },
    {
      &quot;name&quot;: &quot;membercode&quot;,
      &quot;value&quot;: &quot;${membercode}&quot;
    },
    {
      &quot;name&quot;: &quot;memberid&quot;,
      &quot;value&quot;: &quot;${memberid}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${key}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>POST</restRequestMethod>
   <restUrl>${url}/data/lead/add/</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.apiurl</defaultValue>
      <description></description>
      <id>5b11b6a5-975c-48b6-b2bf-c765a51c6fd6</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.tokendms</defaultValue>
      <description></description>
      <id>320fa878-a0ac-43b5-b9dd-b3816debc96d</id>
      <masked>false</masked>
      <name>key</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(10, 2)</defaultValue>
      <description></description>
      <id>b02079e8-8192-495d-970e-52d94e8b236b</id>
      <masked>false</masked>
      <name>leadcarbrand</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(11, 2)</defaultValue>
      <description></description>
      <id>69a27caf-c204-4a8a-b39c-1c87c036052f</id>
      <masked>false</masked>
      <name>leadcarmodel</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(12, 2)</defaultValue>
      <description></description>
      <id>e0d75fce-1614-45e2-8ce7-f5cfd86cf283</id>
      <masked>false</masked>
      <name>leadcarvariant</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(36, 1)</defaultValue>
      <description></description>
      <id>73350fc2-9970-44f0-aad1-8fb084356b1a</id>
      <masked>false</masked>
      <name>leadcarnoteother</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(14, 1)</defaultValue>
      <description></description>
      <id>b5f53f5c-4ff2-455d-8137-839f78faacdf</id>
      <masked>false</masked>
      <name>leadcaryear</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(13, 1)</defaultValue>
      <description></description>
      <id>8ab3101d-43ee-43d1-b4a3-16a017cfefa5</id>
      <masked>false</masked>
      <name>leadcartransmission</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(37, 1)</defaultValue>
      <description></description>
      <id>aa428ba3-2d7d-48cd-8dd7-abb63e259ed1</id>
      <masked>false</masked>
      <name>leadcarkm</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(38, 1)</defaultValue>
      <description></description>
      <id>85e418c2-5328-4ffe-8ffb-f6c6f5155e6f</id>
      <masked>false</masked>
      <name>leadcarprefixplate</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(39, 1)</defaultValue>
      <description></description>
      <id>db4312e7-5399-4f97-ae82-8197e15daa31</id>
      <masked>false</masked>
      <name>leadcarplatenumber</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(40, 1)</defaultValue>
      <description></description>
      <id>4ac6ee13-1c28-42cf-bf82-04eb9a58d5df</id>
      <masked>false</masked>
      <name>leadcarcolour</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(41, 1)</defaultValue>
      <description></description>
      <id>32329cc4-0fad-452f-8d6f-765061370a7e</id>
      <masked>false</masked>
      <name>leadcarcolourother</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(42, 1)</defaultValue>
      <description></description>
      <id>ac244f45-2244-488a-92d6-ddd0b1994ba8</id>
      <masked>false</masked>
      <name>leadstnktaxexpired</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(43, 1)</defaultValue>
      <description></description>
      <id>56062f0d-fc4c-4a2e-a8e6-746e522994ab</id>
      <masked>false</masked>
      <name>leadstnktype</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(44, 1)</defaultValue>
      <description></description>
      <id>c739d72f-bec8-47a5-bd41-2751444b8052</id>
      <masked>false</masked>
      <name>leadfacelift</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(45, 1)</defaultValue>
      <description></description>
      <id>cf211666-a861-424a-94d5-75c51af50ba8</id>
      <masked>false</masked>
      <name>regionid</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(46, 1)</defaultValue>
      <description></description>
      <id>ab5848bc-7be9-494d-ba7a-d0c60982df5c</id>
      <masked>false</masked>
      <name>regionname</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(3, 1)</defaultValue>
      <description></description>
      <id>7a246204-b485-417b-988c-c653de9d21b6</id>
      <masked>false</masked>
      <name>membercode</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(21, 1)</defaultValue>
      <description></description>
      <id>a70c4a6c-fe21-4827-9cdc-7b18c5fbc118</id>
      <masked>false</masked>
      <name>memberid</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
