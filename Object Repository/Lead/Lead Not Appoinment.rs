<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Lead Not Appoinment</name>
   <tag></tag>
   <elementGuidId>17f33d29-b3a7-4074-b11b-4eb012bdc687</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;minimumpricemrp&quot;,
      &quot;value&quot;: &quot;${minimumpricemrp}&quot;
    },
    {
      &quot;name&quot;: &quot;maximumpricemrp&quot;,
      &quot;value&quot;: &quot;${maximumpricemrp}&quot;
    },
    {
      &quot;name&quot;: &quot;pricemrp&quot;,
      &quot;value&quot;: &quot;${pricemrp}&quot;
    },
    {
      &quot;name&quot;: &quot;leadreferencecode&quot;,
      &quot;value&quot;: &quot;${leadreferencecode}&quot;
    },
    {
      &quot;name&quot;: &quot;leadid&quot;,
      &quot;value&quot;: &quot;${leadid}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${key}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${url}/data/lead/appointment/not</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.apiurl</defaultValue>
      <description></description>
      <id>42a735eb-de41-48d6-b955-3d3980e2bb78</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.tokendms</defaultValue>
      <description></description>
      <id>ad5a3e2d-b7cc-464d-963d-2bbd822e812d</id>
      <masked>false</masked>
      <name>key</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(47, 1)</defaultValue>
      <description></description>
      <id>09f274d1-c914-4716-bfe1-9eda67b436a7</id>
      <masked>false</masked>
      <name>minimumpricemrp</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(48, 1)</defaultValue>
      <description></description>
      <id>d78dd931-6906-43aa-8962-035766fd9fe6</id>
      <masked>false</masked>
      <name>maximumpricemrp</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(49, 1)</defaultValue>
      <description></description>
      <id>f2651280-81bf-4892-9104-9d6936acfb50</id>
      <masked>false</masked>
      <name>pricemrp</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(35, 1)</defaultValue>
      <description></description>
      <id>7725b0d7-d911-431d-801d-62672520002e</id>
      <masked>false</masked>
      <name>leadreferencecode</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(50, 1)</defaultValue>
      <description></description>
      <id>4bea5a94-14dc-40f2-b24d-c20e1e48e4c4</id>
      <masked>false</masked>
      <name>leadid</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
