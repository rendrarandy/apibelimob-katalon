<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Lead Status</name>
   <tag></tag>
   <elementGuidId>37ae6b66-6036-4640-a894-5fac0b604417</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;leadstatusname&quot;,
      &quot;value&quot;: &quot;${leadstatusname}&quot;
    },
    {
      &quot;name&quot;: &quot;leadreason&quot;,
      &quot;value&quot;: &quot;${leadreason}&quot;
    },
    {
      &quot;name&quot;: &quot;leadreasonother&quot;,
      &quot;value&quot;: &quot;${leadreasonother}&quot;
    },
    {
      &quot;name&quot;: &quot;leadreferencecode&quot;,
      &quot;value&quot;: &quot;${leadreferencecode}&quot;
    },
    {
      &quot;name&quot;: &quot;leadid&quot;,
      &quot;value&quot;: &quot;${leadid}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${key}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${url}/data//lead/update/status</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.apiurl</defaultValue>
      <description></description>
      <id>88d4aaef-0843-4ff7-80b5-66b2781e18e8</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.tokendms</defaultValue>
      <description></description>
      <id>d1fc2da4-3e7c-4e84-aa88-d864c4fd78e1</id>
      <masked>false</masked>
      <name>key</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(55, 1)</defaultValue>
      <description></description>
      <id>1f18650a-9f60-4a7a-b24f-b8159967ac88</id>
      <masked>false</masked>
      <name>leadstatusname</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(56, 1)</defaultValue>
      <description></description>
      <id>a114dbf6-a47a-4619-b5f0-76071147140d</id>
      <masked>false</masked>
      <name>leadreason</name>
   </variables>
   <variables>
      <defaultValue>findTestData(null).getValue(1, 1)</defaultValue>
      <description></description>
      <id>b1f893a8-76ee-44cc-abb6-c7a235d64ab3</id>
      <masked>false</masked>
      <name>leadreasonother</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(35, 1)</defaultValue>
      <description></description>
      <id>746c47f8-3628-4c1b-b25e-64cf5aef3ae6</id>
      <masked>false</masked>
      <name>leadreferencecode</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(50, 1)</defaultValue>
      <description></description>
      <id>daa0ee92-d76b-411d-bac5-a09b2bf7f2a4</id>
      <masked>false</masked>
      <name>leadid</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(21, 1)</defaultValue>
      <description></description>
      <id>5a5a71e8-d249-4c3c-b450-82af769019e7</id>
      <masked>false</masked>
      <name>memberid</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
