<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Member Img Profile</name>
   <tag></tag>
   <elementGuidId>8307540b-8e31-4c50-8938-119526e83010</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;memberimgprofile&quot;,
      &quot;value&quot;: &quot;${img}&quot;
    },
    {
      &quot;name&quot;: &quot;memberid&quot;,
      &quot;value&quot;: &quot;${memberid}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${key}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${url}/data/member/profile/changeimgprofile</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.apiurl</defaultValue>
      <description></description>
      <id>62a1fb80-0ab3-437b-9f02-f49d223fd27b</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(22, 1)</defaultValue>
      <description></description>
      <id>8118ac67-ed0e-489e-8e65-ec6105c89f77</id>
      <masked>false</masked>
      <name>img</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(21, 1)</defaultValue>
      <description></description>
      <id>c2b22d32-5525-45d2-b3c2-5ffa23241f21</id>
      <masked>false</masked>
      <name>memberid</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.tokendms</defaultValue>
      <description></description>
      <id>5281c985-af30-4a74-a2e9-5f8b4cc6067b</id>
      <masked>false</masked>
      <name>key</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
