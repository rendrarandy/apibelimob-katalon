<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Member Change Password</name>
   <tag></tag>
   <elementGuidId>06fc1421-b3fd-4c0c-9cb6-28250bb40986</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;memberoldpassword&quot;,
      &quot;value&quot;: &quot;${oldpassword}&quot;
    },
    {
      &quot;name&quot;: &quot;memberpassword&quot;,
      &quot;value&quot;: &quot;${password}&quot;
    },
    {
      &quot;name&quot;: &quot;memberconfirmpassword&quot;,
      &quot;value&quot;: &quot;${confirmpassword}&quot;
    },
    {
      &quot;name&quot;: &quot;memberid&quot;,
      &quot;value&quot;: &quot;${memberid}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${key}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${url}/data/member/changepassword/update</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.apiurl</defaultValue>
      <description></description>
      <id>f707782c-f419-4a3b-93d7-3eb6f270e5e7</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.tokendms</defaultValue>
      <description></description>
      <id>c53bf00b-e77f-4e4b-851d-5a2b1b9c434a</id>
      <masked>false</masked>
      <name>key</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(21, 1)</defaultValue>
      <description></description>
      <id>ed04b073-0dae-420b-9842-fe9d8964b138</id>
      <masked>false</masked>
      <name>memberid</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(2, 1)</defaultValue>
      <description></description>
      <id>a00f0619-b1d9-4388-993d-af489ce654d4</id>
      <masked>false</masked>
      <name>oldpassword</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(23, 1)</defaultValue>
      <description></description>
      <id>74cc7d61-14c6-4f21-98c6-4100ffbe9f03</id>
      <masked>false</masked>
      <name>password</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(24, 1)</defaultValue>
      <description></description>
      <id>e96a66ef-32b2-4ffd-a47c-d90f1f75af7f</id>
      <masked>false</masked>
      <name>confirmpassword</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
