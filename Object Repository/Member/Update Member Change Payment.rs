<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Member Change Payment</name>
   <tag></tag>
   <elementGuidId>1d0306a5-7b86-4cc6-b2c7-f139cf6b299c</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;memberbankaccountname&quot;,
      &quot;value&quot;: &quot;${name}&quot;
    },
    {
      &quot;name&quot;: &quot;memberbankaccountnumber&quot;,
      &quot;value&quot;: &quot;${number}&quot;
    },
    {
      &quot;name&quot;: &quot;memberbankname&quot;,
      &quot;value&quot;: &quot;${bankname}&quot;
    },
    {
      &quot;name&quot;: &quot;memberbankcode&quot;,
      &quot;value&quot;: &quot;${bankcode}&quot;
    },
    {
      &quot;name&quot;: &quot;membertaxnumber&quot;,
      &quot;value&quot;: &quot;${taxnumber}&quot;
    },
    {
      &quot;name&quot;: &quot;memberid&quot;,
      &quot;value&quot;: &quot;${memberid}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${key}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${url}/data/member/changepayment/update</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.apiurl</defaultValue>
      <description></description>
      <id>8babdf59-549c-48ca-ad8b-fe78089fcb6f</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.tokendms</defaultValue>
      <description></description>
      <id>335a7a64-5250-4809-927d-d62a5e058893</id>
      <masked>false</masked>
      <name>key</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(25, 1)</defaultValue>
      <description></description>
      <id>cc86e227-52a3-435c-b6c4-be8ee153aaf4</id>
      <masked>false</masked>
      <name>name</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(26, 1)</defaultValue>
      <description></description>
      <id>a34d0b55-da83-44f7-854a-48c6311bb251</id>
      <masked>false</masked>
      <name>number</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(8, 2)</defaultValue>
      <description></description>
      <id>12e45aeb-0d18-44eb-8bf7-c1fafe46fb92</id>
      <masked>false</masked>
      <name>bankname</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(8, 1)</defaultValue>
      <description></description>
      <id>0fc37ea2-0d7f-4e9e-b8d4-28296419c03f</id>
      <masked>false</masked>
      <name>bankcode</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(27, 1)</defaultValue>
      <description></description>
      <id>18a6fce7-dd46-4d93-b378-8409497da8f7</id>
      <masked>false</masked>
      <name>taxnumber</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(21, 1)</defaultValue>
      <description></description>
      <id>f2495927-54c9-4535-b3b7-03bbdc5ff0c2</id>
      <masked>false</masked>
      <name>memberid</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
