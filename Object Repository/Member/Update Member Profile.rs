<?xml version="1.0" encoding="UTF-8"?>
<WebServiceRequestEntity>
   <description></description>
   <name>Update Member Profile</name>
   <tag></tag>
   <elementGuidId>fe1e423d-8bee-4db6-9182-acf02961f51e</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <httpBody></httpBody>
   <httpBodyContent>{
  &quot;contentType&quot;: &quot;application/x-www-form-urlencoded&quot;,
  &quot;charset&quot;: &quot;UTF-8&quot;,
  &quot;parameters&quot;: [
    {
      &quot;name&quot;: &quot;memberaddress&quot;,
      &quot;value&quot;: &quot;${address}&quot;
    },
    {
      &quot;name&quot;: &quot;membernoktp&quot;,
      &quot;value&quot;: &quot;${noktp}&quot;
    },
    {
      &quot;name&quot;: &quot;membercompany&quot;,
      &quot;value&quot;: &quot;${company}&quot;
    },
    {
      &quot;name&quot;: &quot;memberposition&quot;,
      &quot;value&quot;: &quot;${position}&quot;
    },
    {
      &quot;name&quot;: &quot;memberotherposition&quot;,
      &quot;value&quot;: &quot;${otherposition}&quot;
    },
    {
      &quot;name&quot;: &quot;membermedsosfacebook&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;membermedsostwitter&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;membermedsosinstagram&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;membermedsosgoogleplus&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;membermedsoslinkedin&quot;,
      &quot;value&quot;: &quot;&quot;
    },
    {
      &quot;name&quot;: &quot;memberid&quot;,
      &quot;value&quot;: &quot;${memberid}&quot;
    }
  ]
}</httpBodyContent>
   <httpBodyType>x-www-form-urlencoded</httpBodyType>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Content-Type</name>
      <type>Main</type>
      <value>application/x-www-form-urlencoded</value>
   </httpHeaderProperties>
   <httpHeaderProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>Authorization</name>
      <type>Main</type>
      <value>${key}</value>
   </httpHeaderProperties>
   <migratedVersion>5.4.1</migratedVersion>
   <restRequestMethod>PUT</restRequestMethod>
   <restUrl>${url}/data/member/profile/update</restUrl>
   <serviceType>RESTful</serviceType>
   <soapBody></soapBody>
   <soapHeader></soapHeader>
   <soapRequestMethod></soapRequestMethod>
   <soapServiceFunction></soapServiceFunction>
   <variables>
      <defaultValue>GlobalVariable.apiurl</defaultValue>
      <description></description>
      <id>4a925a04-6a0d-4868-89f5-c77dc6fecd38</id>
      <masked>false</masked>
      <name>url</name>
   </variables>
   <variables>
      <defaultValue>GlobalVariable.tokendms</defaultValue>
      <description></description>
      <id>7f568726-3e45-4715-be96-63f933f9331d</id>
      <masked>false</masked>
      <name>key</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(21, 1)</defaultValue>
      <description></description>
      <id>4e33a754-84f0-4b1e-b8e5-53ee6236d64d</id>
      <masked>false</masked>
      <name>memberid</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(28, 1)</defaultValue>
      <description></description>
      <id>172e0cd0-131c-4124-a8a6-f35fde037c37</id>
      <masked>false</masked>
      <name>address</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(29, 1)</defaultValue>
      <description></description>
      <id>a90ee4c0-ce61-4f4d-b045-12770b88d52b</id>
      <masked>false</masked>
      <name>noktp</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(30, 1)</defaultValue>
      <description></description>
      <id>dba5d3b3-84cc-4115-8ca6-c89d78de6ed9</id>
      <masked>false</masked>
      <name>company</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(31, 1)</defaultValue>
      <description></description>
      <id>94430035-6834-4124-8dc2-333d7907145c</id>
      <masked>false</masked>
      <name>position</name>
   </variables>
   <variables>
      <defaultValue>findTestData('Auth/Data Login').getValue(32, 1)</defaultValue>
      <description></description>
      <id>06426d74-06c3-43d7-b36e-92a84f77259a</id>
      <masked>false</masked>
      <name>otherposition</name>
   </variables>
   <verificationScript>import static org.assertj.core.api.Assertions.*

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webservice.verification.WSResponseManager

import groovy.json.JsonSlurper
import internal.GlobalVariable as GlobalVariable

RequestObject request = WSResponseManager.getInstance().getCurrentRequest()

ResponseObject response = WSResponseManager.getInstance().getCurrentResponse()
</verificationScript>
   <wsdlAddress></wsdlAddress>
</WebServiceRequestEntity>
