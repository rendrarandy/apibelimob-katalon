import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

response = WS.sendRequest(findTestObject('Member/Update Member Change Payment', [('url') : GlobalVariable.apiurl, ('key') : GlobalVariable.tokendms
            , ('name') : findTestData('Auth/Data Login').getValue(25, 1), ('number') : findTestData('Auth/Data Login').getValue(
                26, 1), ('bankname') : findTestData('Auth/Data Login').getValue(8, 2), ('bankcode') : findTestData('Auth/Data Login').getValue(
                8, 1), ('taxnumber') : findTestData('Auth/Data Login').getValue(27, 1), ('memberid') : findTestData('Auth/Data Login').getValue(
                21, 1)]))

WS.verifyResponseStatusCode(response, 200)

WS.verifyElementPropertyValue(response, 'status', 'true')

