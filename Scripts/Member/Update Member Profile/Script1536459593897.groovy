import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

response = WS.sendRequest(findTestObject('Member/Update Member Profile', [('url') : GlobalVariable.apiurl, ('key') : GlobalVariable.tokendms
            , ('memberid') : findTestData('Auth/Data Login').getValue(21, 1), ('address') : findTestData('Auth/Data Login').getValue(
                28, 1), ('noktp') : findTestData('Auth/Data Login').getValue(29, 1), ('company') : findTestData('Auth/Data Login').getValue(
                30, 1), ('position') : findTestData('Auth/Data Login').getValue(31, 1), ('otherposition') : findTestData(
                'Auth/Data Login').getValue(32, 1)]))

WS.verifyResponseStatusCode(response, 200)

WS.verifyElementPropertyValue(response, 'status', 'true')

