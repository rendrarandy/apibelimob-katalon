import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

response = WS.sendRequest(findTestObject('Lead/Add Lead', [('url') : GlobalVariable.apiurl, ('key') : GlobalVariable.tokendms
            , ('leadcarbrand') : findTestData('Auth/Data Login').getValue(10, 2), ('leadcarmodel') : findTestData('Auth/Data Login').getValue(
                11, 2), ('leadcarvariant') : findTestData('Auth/Data Login').getValue(12, 2), ('leadcarnoteother') : findTestData(
                'Auth/Data Login').getValue(36, 1), ('leadcaryear') : findTestData('Auth/Data Login').getValue(14, 1), ('leadcartransmission') : findTestData(
                'Auth/Data Login').getValue(13, 1), ('leadcarkm') : findTestData('Auth/Data Login').getValue(37, 1), ('leadcarprefixplate') : findTestData(
                'Auth/Data Login').getValue(38, 1), ('leadcarplatenumber') : findTestData('Auth/Data Login').getValue(39, 
                1), ('leadcarcolour') : findTestData('Auth/Data Login').getValue(40, 1), ('leadcarcolourother') : findTestData(
                'Auth/Data Login').getValue(41, 1), ('leadstnktaxexpired') : findTestData('Auth/Data Login').getValue(42, 
                1), ('leadstnktype') : findTestData('Auth/Data Login').getValue(43, 1), ('leadfacelift') : findTestData(
                'Auth/Data Login').getValue(44, 1), ('regionid') : findTestData('Auth/Data Login').getValue(45, 1), ('regionname') : findTestData(
                'Auth/Data Login').getValue(46, 1), ('membercode') : findTestData('Auth/Data Login').getValue(3, 1), ('memberid') : findTestData(
                'Auth/Data Login').getValue(21, 1)]))

WS.verifyResponseStatusCode(response, 200)

WS.verifyElementPropertyValue(response, 'status', 'true')

