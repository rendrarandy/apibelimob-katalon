import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable

response = WS.sendRequest(findTestObject('Lead/Lead Yes Appoinment', [('url') : GlobalVariable.apiurl, ('key') : GlobalVariable.tokendms
            , ('minimumpricemrp') : findTestData('Auth/Data Login').getValue(47, 1), ('maximumpricemrp') : findTestData(
                'Auth/Data Login').getValue(48, 1), ('pricemrp') : findTestData('Auth/Data Login').getValue(49, 1), ('leadappointmentdate') : findTestData(
                'Auth/Data Login').getValue(51, 1), ('leadappointmentaddress') : findTestData('Auth/Data Login').getValue(
                52, 1), ('leadappointmentprovince') : findTestData('Auth/Data Login').getValue(4, 2), ('leadappointmentcity') : findTestData(
                'Auth/Data Login').getValue(5, 2), ('leadappointmentdistrict') : findTestData('Auth/Data Login').getValue(
                6, 2), ('leadappointmentbuilding') : findTestData('Auth/Data Login').getValue(53, 1), ('leadappointmentremark') : findTestData(
                'Auth/Data Login').getValue(54, 1), ('leadreferencecode') : findTestData('Auth/Data Login').getValue(35, 
                1), ('leadid') : findTestData('Auth/Data Login').getValue(50, 1)]))

WS.verifyResponseStatusCode(response, 200)

WS.verifyElementPropertyValue(response, 'status', 'true')

