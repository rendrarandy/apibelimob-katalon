<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>all test lead</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>d06b73e4-c33d-4ae5-b437-f6a048d7aef5</testSuiteGuid>
   <testCaseLink>
      <guid>f725b394-7311-4168-a487-be330e332c13</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lead/Add Lead</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f65d6325-ce75-4b54-94fb-63b0b600a118</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lead/Count Lead Dashboard</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e85c4768-aa45-4bfd-9292-77dca5d9a618</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lead/Data All Lead Member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>67a0c148-b81c-432d-8c32-7fddc3b2d138</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lead/Data Detail Lead Member</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66ac9ca9-969e-44b8-88a6-623f5048e667</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lead/Lead Not Appoinment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>22bed3d6-15e3-4371-9158-2fc060eba959</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lead/Lead Yes Appoinment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d59247f-0596-4c3b-a242-370df55f8104</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Lead/Update Lead Status</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
