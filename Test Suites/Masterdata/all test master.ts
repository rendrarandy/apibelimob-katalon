<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>all test master</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-09-10T11:40:41</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>0aabbf19-eb19-4198-a2b0-bf5793fbef31</testSuiteGuid>
   <testCaseLink>
      <guid>91e3a71a-fe36-4a9e-97fe-86ce0194c0ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Masterdata/Bank</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0f21b07d-a6a4-491d-b583-4723cdeb3b5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Masterdata/Lead Reason</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13e9740c-7b66-4fc1-97c0-fb1aa59d6264</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Masterdata/Member Position</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1aeaa2c0-61ff-4d4b-a97a-9b6a81eb11d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Masterdata/Car Colour</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>448c8ea5-2375-4ee6-a6d6-adf0f175b641</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Masterdata/Car Prefix Plate</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fdb66f2c-e8ce-41c0-ad87-16023eb23e79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Masterdata/Car Transmission</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1aa15bd7-923d-4690-b03b-fa1ba15a75d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Masterdata/Province</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a802629e-df9c-4aaa-a0f1-2bc107727bfb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Masterdata/Car Model</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0713f6f6-86c9-4ec8-82df-db0a65b01faf</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Masterdata/Car Variant</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b081ac75-bbdc-42d4-9a1e-9ce13ef0336c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Masterdata/City</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3e1dfe38-92c1-41c3-8566-946d94b0eae1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Masterdata/District</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
